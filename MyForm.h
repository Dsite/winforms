#pragma once

#include <stdio.h>
#include <string>
#include <iostream>
#include <fstream>
#include "PreferencesForm.h"

namespace PIngPong {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
			timer1->Enabled = false;
		//	lblName->Text = datarecev.ToString();
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::PictureBox^  picBoxMap;
	protected:
	private: System::Windows::Forms::PictureBox^  picBoxPlayer;
	private: System::Windows::Forms::PictureBox^  picBoxBall;

	private:

	private: System::Windows::Forms::Timer^  timer1;
	private: System::Windows::Forms::Label^  lblScore;

	private: System::Windows::Forms::PictureBox^  picBoxBlock1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Label^  lblName;
	private: System::Windows::Forms::Label^  lblGOver;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  lblSpeed;
	private: System::Windows::Forms::Button^  btnPrefs;
	private: System::ComponentModel::IContainer^  components;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->picBoxMap = (gcnew System::Windows::Forms::PictureBox());
			this->picBoxPlayer = (gcnew System::Windows::Forms::PictureBox());
			this->picBoxBall = (gcnew System::Windows::Forms::PictureBox());
			this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
			this->lblScore = (gcnew System::Windows::Forms::Label());
			this->picBoxBlock1 = (gcnew System::Windows::Forms::PictureBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->lblName = (gcnew System::Windows::Forms::Label());
			this->lblGOver = (gcnew System::Windows::Forms::Label());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->lblSpeed = (gcnew System::Windows::Forms::Label());
			this->btnPrefs = (gcnew System::Windows::Forms::Button());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->picBoxMap))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->picBoxPlayer))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->picBoxBall))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->picBoxBlock1))->BeginInit();
			this->SuspendLayout();
			// 
			// picBoxMap
			// 
			this->picBoxMap->BackColor = System::Drawing::SystemColors::ActiveCaption;
			this->picBoxMap->Location = System::Drawing::Point(2, 61);
			this->picBoxMap->Name = L"picBoxMap";
			this->picBoxMap->Size = System::Drawing::Size(1126, 621);
			this->picBoxMap->TabIndex = 0;
			this->picBoxMap->TabStop = false;
			this->picBoxMap->MouseMove += gcnew System::Windows::Forms::MouseEventHandler(this, &MyForm::Map_MouseMove);
			// 
			// picBoxPlayer
			// 
			this->picBoxPlayer->BackColor = System::Drawing::SystemColors::ActiveCaptionText;
			this->picBoxPlayer->Location = System::Drawing::Point(500, 629);
			this->picBoxPlayer->Name = L"picBoxPlayer";
			this->picBoxPlayer->Size = System::Drawing::Size(130, 15);
			this->picBoxPlayer->TabIndex = 1;
			this->picBoxPlayer->TabStop = false;
			// 
			// picBoxBall
			// 
			this->picBoxBall->BackColor = System::Drawing::Color::Red;
			this->picBoxBall->Location = System::Drawing::Point(511, 390);
			this->picBoxBall->Name = L"picBoxBall";
			this->picBoxBall->Size = System::Drawing::Size(20, 20);
			this->picBoxBall->TabIndex = 2;
			this->picBoxBall->TabStop = false;
			// 
			// timer1
			// 
			this->timer1->Interval = 20;
			this->timer1->Tick += gcnew System::EventHandler(this, &MyForm::timer1_Tick);
			// 
			// lblScore
			// 
			this->lblScore->AutoSize = true;
			this->lblScore->Location = System::Drawing::Point(464, 26);
			this->lblScore->Name = L"lblScore";
			this->lblScore->Size = System::Drawing::Size(16, 17);
			this->lblScore->TabIndex = 3;
			this->lblScore->Text = L"0";
			// 
			// picBoxBlock1
			// 
			this->picBoxBlock1->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)), static_cast<System::Int32>(static_cast<System::Byte>(192)),
				static_cast<System::Int32>(static_cast<System::Byte>(0)));
			this->picBoxBlock1->Location = System::Drawing::Point(181, 206);
			this->picBoxBlock1->Name = L"picBoxBlock1";
			this->picBoxBlock1->Size = System::Drawing::Size(95, 35);
			this->picBoxBlock1->TabIndex = 4;
			this->picBoxBlock1->TabStop = false;
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(873, 26);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(202, 17);
			this->label2->TabIndex = 5;
			this->label2->Text = L"ESC - Exit    Mouse - Left/Right";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(409, 26);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(49, 17);
			this->label3->TabIndex = 6;
			this->label3->Text = L"Score:";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(192, 26);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(74, 17);
			this->label4->TabIndex = 7;
			this->label4->Text = L"Nickname:";
			// 
			// lblName
			// 
			this->lblName->AutoSize = true;
			this->lblName->Location = System::Drawing::Point(272, 26);
			this->lblName->Name = L"lblName";
			this->lblName->Size = System::Drawing::Size(0, 17);
			this->lblName->TabIndex = 8;
			// 
			// lblGOver
			// 
			this->lblGOver->AutoSize = true;
			this->lblGOver->BackColor = System::Drawing::SystemColors::ActiveCaption;
			this->lblGOver->Font = (gcnew System::Drawing::Font(L"Minion Pro", 25.8F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->lblGOver->Location = System::Drawing::Point(435, 123);
			this->lblGOver->Name = L"lblGOver";
			this->lblGOver->Size = System::Drawing::Size(230, 240);
			this->lblGOver->TabIndex = 9;
			this->lblGOver->Text = L"Game Over\r\n\r\nESC - Exit\r\n\r\n";
			this->lblGOver->Visible = false;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(12, 26);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(57, 17);
			this->label1->TabIndex = 10;
			this->label1->Text = L"Speed: ";
			this->label1->Visible = false;
			// 
			// lblSpeed
			// 
			this->lblSpeed->AutoSize = true;
			this->lblSpeed->Location = System::Drawing::Point(75, 26);
			this->lblSpeed->Name = L"lblSpeed";
			this->lblSpeed->Size = System::Drawing::Size(16, 17);
			this->lblSpeed->TabIndex = 11;
			this->lblSpeed->Text = L"0";
			this->lblSpeed->Visible = false;
			// 
			// btnPrefs
			// 
			this->btnPrefs->Location = System::Drawing::Point(627, 20);
			this->btnPrefs->Name = L"btnPrefs";
			this->btnPrefs->Size = System::Drawing::Size(127, 23);
			this->btnPrefs->TabIndex = 12;
			this->btnPrefs->Text = L"Prefferences";
			this->btnPrefs->UseVisualStyleBackColor = true;
			this->btnPrefs->Click += gcnew System::EventHandler(this, &MyForm::btnPrefs_Click);
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(1129, 682);
			this->Controls->Add(this->btnPrefs);
			this->Controls->Add(this->lblSpeed);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->lblGOver);
			this->Controls->Add(this->lblName);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->picBoxBlock1);
			this->Controls->Add(this->lblScore);
			this->Controls->Add(this->picBoxBall);
			this->Controls->Add(this->picBoxPlayer);
			this->Controls->Add(this->picBoxMap);
			this->Name = L"MyForm";
			this->Text = L"MyForm";
			this->Load += gcnew System::EventHandler(this, &MyForm::MyForm_Load);
			this->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &MyForm::MyForm_KeyDown);
			this->MouseMove += gcnew System::Windows::Forms::MouseEventHandler(this, &MyForm::MyForm_MouseMove);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->picBoxMap))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->picBoxPlayer))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->picBoxBall))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->picBoxBlock1))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion				
								//-------------- to do: okno do podawania parapetrow gry, zapisywanie, level, bloki + unac picBoxGame i dac na formie
		int speedLeft = 4;	// horizontal speed of ball
		int speedTop = 4;	// vertical speed of ball
		int timTick = 0;	// timer tick (interval)
		int point = 0;		// score
	private: System::Void MyForm_Load(System::Object^  sender, System::EventArgs^  e) {
		//picBoxPlayer->Left = (picBoxMap->Size.Width / 2) - 50;
		//picBoxPlayer->Top = picBoxMap->Size.Height -10;
	}
	private: System::Void MyForm_KeyDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e) {
		if (e->KeyCode == Keys::Escape)
		{
			Close();
		}
	}
private: System::Void MyForm_MouseMove(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
	
}
private: System::Void Map_MouseMove(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
	
}
private: System::Void timer1_Tick(System::Object^  sender, System::EventArgs^  e) {
	timTick += 1;
	picBoxBall->Left += speedLeft;
	picBoxBall->Top += speedTop;
	
	
	picBoxPlayer->Left = Cursor->Position.X - 50;
	if (picBoxBall->Bottom >= picBoxPlayer->Top && picBoxBall->Top <= picBoxPlayer->Bottom && picBoxBall->Right>= picBoxPlayer->Left && picBoxBall->Right<= picBoxPlayer->Right)
	{					//	collision ball -> player
		speedLeft += 1;
		speedTop += 1;
		speedTop = -speedTop;
		point += 5;
		lblScore->Text = point.ToString();
		lblSpeed->Text = speedLeft.ToString();
	}
	if (picBoxBall->Left <= picBoxMap->Left)		// collision ball -> frame
		speedLeft = -speedLeft;
	if (picBoxBall->Right >= picBoxMap->Right)
		speedLeft = -speedLeft;
	if (picBoxBall->Top <= picBoxMap->Top)
		speedTop = -speedTop;
	
	if (timTick >= 50 && picBoxBlock1->Enabled==true)
	{
		
		
		if (picBoxBall->Bottom >= picBoxBlock1->Top && picBoxBall->Top <= picBoxBlock1->Bottom && picBoxBall->Right >= picBoxBlock1->Left && picBoxBall->Right <= picBoxBlock1->Right)
		{											// collision ball -> blocks
			picBoxBlock1->Enabled = false;
			picBoxBlock1->Visible = false;
			point += 10;
			lblScore->Text = point.ToString();
		}

	}
	if (picBoxBall->Top >= picBoxPlayer->Bottom)
	{
		lblGOver->Visible = true;
		timer1->Enabled = false;
	}
}
		 //Form^ okno = gcnew Form;
private: System::Void btnPrefs_Click(System::Object^  sender, System::EventArgs^  e) {
	
	Form^ okno = gcnew Form;
	okno->Width = 350;
	okno->Height = 280;
	okno->Name = "Prefferences";

	Label^ lblTitile = gcnew Label;
	okno->Controls->Add(lblTitile);
	lblTitile->Location = Point(12,9);
	lblTitile->Text = "Set Preferences";
	
	Label^ lblBallSpeed = gcnew Label;
	lblBallSpeed->Text = "Ball Speed";
	lblBallSpeed->Location = Point(19, 60);
	okno->Controls->Add(lblBallSpeed);

	Label^ lblNickname = gcnew Label;
	lblNickname->Text = "Nickname";
	lblNickname->Location = Point(19, 116);
	okno->Controls->Add(lblNickname);

	ComboBox^ cmbBoxBallSpeed = gcnew ComboBox;
	cmbBoxBallSpeed->DropDownWidth = 131;
	cmbBoxBallSpeed->Size.Width = 131;
	cmbBoxBallSpeed->Location = Point(19, 84);
	for(int i=1;i<10;i++)
	cmbBoxBallSpeed->Items->Add(i);
	okno->Controls->Add(cmbBoxBallSpeed);

	TextBox^ txtBoxNickname = gcnew TextBox;
	txtBoxNickname->Size.Width = 131;
	txtBoxNickname->Location = Point(19, 140);
	okno->Controls->Add(txtBoxNickname);

	Button^ btnStart = gcnew Button;
	okno->Controls->Add(btnStart);
	btnStart->Text = "OK";
	btnStart->Location = Point(207, 94);
	btnStart->DialogResult = System::Windows::Forms::DialogResult::OK;
	okno->ShowDialog();
	lblName->Text = txtBoxNickname->Text;
	timer1->Enabled = true;
	okno->Close();


	Button^ btnExit = gcnew Button;
	okno->Controls->Add(btnExit);
	btnExit->Text = "Exit";
	btnExit->Location = Point(207, 124);

	

	//okno->Show();

	//txtBoxNickname->Text = this->lblName->Text;
}
		/* public: System::Void btnStart_Click(System::Object^ sender, System::EventArgs^ e)
		 {
			 this->Controls->Add(this->lblName);
			 okno->Close();
		 }*/
};
}
